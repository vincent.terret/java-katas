package org.javakatas.berlinclock;

import java.time.LocalTime;

import org.javakatas.berlinclock.BerlinResult.BerlinResultAggregator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.AggregateWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test conversion from a local time to a berlin time
 *
 * Created by vterret on 22/07/2019.
 *
 * @author vterret
 * @version 1.0.0, 22/07/2019
 */
@DisplayName("Conversion test from digital time to berlin time")
class TestBerlinConverter1 {

  @Tag("Minutes")
  @ParameterizedTest
  @CsvFileSource(resources = "berlinConverter.csv")
  void testSingleMinutesRow(@AggregateWith(BerlinResultAggregator.class) BerlinResult berlinResult) {
    // Given I have started the converter
    BerlinConverter converter = new BerlinConverter();

    // When I enter $time
    LocalTime time = LocalTime.of(berlinResult.getHours(), berlinResult.getMinutes(), berlinResult.getSeconds());

    // Then $row is returned for the single minutes row
    assertEquals(berlinResult.getSingleMinutes(), converter.getSingleMinutesRow(time));
  }

  @Tag("Minutes")
  @ParameterizedTest
  @CsvFileSource(resources = "berlinConverter.csv")
  void testFiveMinutesRow(@AggregateWith(BerlinResultAggregator.class) BerlinResult berlinResult) {
    // Given I have started the converter
    BerlinConverter converter = new BerlinConverter();

    // When I enter $time
    LocalTime time = LocalTime.of(berlinResult.getHours(), berlinResult.getMinutes(), berlinResult.getSeconds());

    // Then $row is returned for the five minutes row
    assertEquals(berlinResult.getFiveMinutes(), converter.getFiveMinutesRow(time));
  }

  @Tag("Hours")
  @ParameterizedTest
  @CsvFileSource(resources = "berlinConverter.csv")
  void testSingleHoursRow(@AggregateWith(BerlinResultAggregator.class) BerlinResult berlinResult) {
    // Given I have started the converter
    BerlinConverter converter = new BerlinConverter();

    // When I enter $time
    LocalTime time = LocalTime.of(berlinResult.getHours(), berlinResult.getMinutes(), berlinResult.getSeconds());

    // Then $row is returned for the five minutes row
    assertEquals(berlinResult.getSingleHours(), converter.getSingleHoursRow(time));
  }

  @Tag("Hours")
  @ParameterizedTest
  @CsvFileSource(resources = "berlinConverter.csv")
  void testFiveHoursRow(@AggregateWith(BerlinResultAggregator.class) BerlinResult berlinResult) {
    // Given I have started the converter
    BerlinConverter converter = new BerlinConverter();

    // When I enter $time
    LocalTime time = LocalTime.of(berlinResult.getHours(), berlinResult.getMinutes(), berlinResult.getSeconds());

    // Then $row is returned for the five minutes row
    assertEquals(berlinResult.getFiveHours(), converter.getFiveHoursRow(time));
  }

  @Tag("Seconds")
  @ParameterizedTest
  @CsvFileSource(resources = "berlinConverter.csv")
  void testSecondsRow(@AggregateWith(BerlinResultAggregator.class) BerlinResult berlinResult) {
    // Given I have started the converter
    BerlinConverter converter = new BerlinConverter();

    // When I enter $time
    LocalTime time = LocalTime.of(berlinResult.getHours(), berlinResult.getMinutes(), berlinResult.getSeconds());

    // Then $row is returned for the five minutes row
    assertEquals(berlinResult.getSecondsLamp(), converter.getSecondsRow(time));
  }

  @Tag("Clock")
  @ParameterizedTest
  @CsvFileSource(resources = "berlinConverter.csv")
  void testClockRow(@AggregateWith(BerlinResultAggregator.class) BerlinResult berlinResult) {
    // Given I have started the converter
    BerlinConverter converter = new BerlinConverter();

    // When I enter $time
    LocalTime time = LocalTime.of(berlinResult.getHours(), berlinResult.getMinutes(), berlinResult.getSeconds());

    // Then $row is returned for the five minutes row
    assertEquals(berlinResult.getBerlinTime(), converter.getBerlinTime(time));
  }
}
