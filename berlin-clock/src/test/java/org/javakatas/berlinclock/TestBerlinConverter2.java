package org.javakatas.berlinclock;

import java.time.LocalTime;

import org.javakatas.berlinclock.BerlinResult.BerlinResultAggregator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.AggregateWith;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test conversion from a berlin time to a local time
 *
 * Created by vterret on 22/07/2019.
 *
 * @author vterret
 * @version 1.0.0, 22/07/2019
 */
@DisplayName("Conversion test from berlin time time to digital")
class TestBerlinConverter2 {

  @ParameterizedTest
  @CsvFileSource(resources = "berlinConverter.csv")
  void testBerlinClockToDigital(@AggregateWith(BerlinResultAggregator.class) BerlinResult berlinResult) {
    // Given I have started the converter
    BerlinConverter converter = new BerlinConverter();

    // When I enter a $berlinTime
    String berlinTime = berlinResult.getBerlinTime();

    // Then $digitalTime is returned
    assertEquals(LocalTime.of(berlinResult.getHours(), berlinResult.getMinutes()), converter.getDigitalTime(berlinTime));
  }

  @ParameterizedTest
  @NullAndEmptySource
  void testNullOrEmpty(String berlinTime) {
    // Given I have started the converter
    BerlinConverter converter = new BerlinConverter();

    // When I enter null or empty as $berlinTime

    // An IllegalArgumentException is thrown
    assertThrows(IllegalArgumentException.class, () -> converter.getDigitalTime(berlinTime));
  }

  @Test
  void testInvalidInput() {
    // Given I have started the converter
    BerlinConverter converter = new BerlinConverter();

    // When I enter an invalid String as $berlinTime
    String berlinTime = "RROY";

    // An IllegalArgumentException is thrown
    assertThrows(IllegalArgumentException.class, () -> converter.getDigitalTime(berlinTime));
  }

}
