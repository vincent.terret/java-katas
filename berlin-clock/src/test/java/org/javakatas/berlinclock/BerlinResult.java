package org.javakatas.berlinclock;

import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregationException;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;

/**
 * Simple POJO for parametrized tests.
 * The inner class {@link BerlinResultAggregator} is used for transform csv file into more comfortable POJO.
 *
 * Created by vterret on 23/07/2019.
 *
 * @author vterret
 * @version 1.0.0, 23/07/2019
 */
class BerlinResult {

  private final int hours;
  private final int minutes;
  private final int seconds;
  private final String singleMinutes;
  private final String fiveMinutes;
  private final String singleHours;
  private final String fiveHours;
  private final String secondsLamp;
  private final String berlinTime;

  private BerlinResult(int hours, int minutes, int seconds, String singleMinutes, String fiveMinutes, String singleHours, String fiveHours,
      String secondsLamp, String berlinTime) {
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;
    this.singleMinutes = singleMinutes;
    this.fiveMinutes = fiveMinutes;
    this.singleHours = singleHours;
    this.fiveHours = fiveHours;
    this.secondsLamp = secondsLamp;
    this.berlinTime = berlinTime;
  }

  int getHours() {
    return hours;
  }

  int getMinutes() {
    return minutes;
  }

  int getSeconds() {
    return seconds;
  }

  String getSingleMinutes() {
    return singleMinutes;
  }

  String getFiveMinutes() {
    return fiveMinutes;
  }

  String getSingleHours() {
    return singleHours;
  }

  String getFiveHours() {
    return fiveHours;
  }

  String getSecondsLamp() {
    return secondsLamp;
  }

  String getBerlinTime() {
    return berlinTime;
  }

  static class BerlinResultAggregator implements ArgumentsAggregator {
    @Override
    public BerlinResult aggregateArguments(ArgumentsAccessor accessor, ParameterContext context) throws ArgumentsAggregationException {
      return new BerlinResult(
          accessor.getInteger(0),
          accessor.getInteger(1),
          accessor.getInteger(2),
          accessor.getString(3),
          accessor.getString(4),
          accessor.getString(5),
          accessor.getString(6),
          accessor.getString(7),
          accessor.getString(8));
    }
  }

}
