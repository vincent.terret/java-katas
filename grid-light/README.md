# Grid Light Kata

You want to control a grid of leds by simple commands. 
I made this kata for working on functional interface.

## Feature1 : All or nothing

A led can be on or off. Then 4 commands are necessary to pilot the system :
- Create a grid
- Turn on all leds between to points of the grid => turnOn(x1,y1,x2,y2)
- Turn off all leds between to points of the grid => turnOff(x1,y1,x2,y2)
- Toggle all led between to points of the grid => toggle(x1,y1,x2,y2)

 Complete the [Grid1](src/main/java/org/javakatas/gridlight/Grid1.java) and pass all test on [Grid1Test](src/test/java/org/javakatas/gridlight/Grid1Test.java)

## Feature2 : Dimmable leds

In this case a led can have a brightness with a value between 0 and 255. 
- Create a grid
- Increase brightness of a certain value for all leds between to points of the grid => increase(x1,y1,x2,y2,inc)
- Decrease brightness of a certain value for all leds between to points of the grid => increase(x1,y1,x2,y2,dec)

 Complete the [Grid2](src/main/java/org/javakatas/gridlight/Grid2.java) and pass all test on [Grid2Test](src/test/java/org/javakatas/gridlight/Grid2Test.java)
