package org.javakatas.gridlight;

/**
 * Created by vterret on 08/08/2019.
 *
 * @author vterret
 * @version 1.0.0, 08/08/2019
 */
@FunctionalInterface
public interface Grid2 {

  int[][] get();

  static Grid2 of(int x, int y) {
    return () -> null;
  }

  /**
   * turn on (or leave on) every light inside parameters
   */
  default Grid2 increase(int x1, int y1, int x2, int y2, int value) {
    return () -> null;
  }

  /**
   * turn off (or leave on) every light inside parameters
   */
  default Grid2 decrease(int x1, int y1, int x2, int y2, int value) {
    return () -> null;
  }

}
