package org.javakatas.gridlight;

/**
 * Created by vterret on 08/08/2019.
 *
 * @author vterret
 * @version 1.0.0, 08/08/2019
 */
@FunctionalInterface
public interface Grid1 {

  int[][] get();

  static Grid1 of(int x, int y) {
    return () -> new int[0][0];
  }

  /**
   * turn on (or leave on) every light inside parameters
   */
  default Grid1 turnOn(int x1, int y1, int x2, int y2) {
    return () -> null;
  }

  /**
   * turn off (or leave on) every light inside parameters
   */
  default Grid1 turnOff(int x1, int y1, int x2, int y2) {
    return () -> null;
  }

  default Grid1 toggle(int x1, int y1, int x2, int y2) {
    return () -> null;
  }
}
