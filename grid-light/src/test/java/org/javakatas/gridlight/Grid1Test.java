package org.javakatas.gridlight;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Created by vterret on 08/08/2019.
 *
 * @author vterret
 * @version 1.0.0, 08/08/2019
 */
class Grid1Test {

  /**
   * Test creation with invalid values
   */
  @ParameterizedTest
  @CsvSource(value = { "0,10", "10,0", "-1,10", "10,-1" })
  void initialValidation(int x, int y) {
    // When a grid is created with a size less or equals to 0
    // Then an IllegalArgumentException is thrown immediately
    assertThrows(IllegalArgumentException.class, () -> Grid1.of(x, y));
  }

  /**
   * Verify correct initialization with different size
   */
  @ParameterizedTest
  @CsvSource(value = { "10,10", "10,20", "1,1", "20,10" })
  void creation(int x, int y) {
    // Given a 10*10 grid
    Grid1 grid = Grid1.of(x, y);

    // When nothing is done
    int[][] lights = grid.get();

    // Then returned grid has correct size
    assertThat(lights.length, is(x));
    assertThat(lights[0].length, is(y));

    // And all light are off
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(0, i));
  }

  @Test
  void turnOnAll() {
    // Given a 10*10 grid
    Grid1 grid = Grid1.of(10, 10);

    // When you turns all light on and get the result
    int[][] lights = grid.turnOn(0, 0, 9, 9).get();

    // Then all light are on
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(1, i));
  }

  @Test
  void turnOnOne() {
    // Given a 10*10 grid
    Grid1 grid = Grid1.of(10, 10);

    // When you turns first light on and get the result
    int[][] lights = grid.turnOn(0, 0, 0, 0).get();

    // Then except the first one all light are off
    assertThat(lights[0][0], is(1));
    Arrays.stream(lights).flatMapToInt(Arrays::stream).skip(1).forEach(i -> assertEquals(0, i));
  }

  @Test
  void turnOnAlreadyOn() {
    // Given a 10*10 grid with first light turned on
    Grid1 grid = Grid1.of(10, 10)
                      .turnOn(0, 0, 0, 0);

    // When you turn on again the first light and get the result
    int[][] lights = grid.turnOn(0, 0, 0, 0).get();

    // Then except the first one all light are off
    assertThat(lights[0][0], is(1));
    Arrays.stream(lights).flatMapToInt(Arrays::stream).skip(1).forEach(i -> assertEquals(0, i));
  }

  @ParameterizedTest
  @CsvSource(value = { "4,4,5,5", "5,5,4,4", "4,5,5,4", "5,4,4,5", })
  void turnOnInvarient(int x1, int y1, int x2, int y2) {
    // Given a 10*10 grid
    Grid1 grid = Grid1.of(10, 10);

    // When you turn on with parameters in any order
    int[][] lights = grid.turnOn(x1, y1, x2, y2).get();

    // Then result is the same
    assertThat(countLightsOn(lights), is(4));
  }

  @ParameterizedTest
  @CsvSource(value = { "-1,0,0,0", "0,-1,0,0", "0,0,-1,0", "0,0,0,-1", "11,0,0,0", "0,21,0,0", "0,0,11,0", "0,0,0,21" })
  void turnOnOutsideGrid(int x1, int y1, int x2, int y2) {
    // Given a 10*20 grid
    Grid1 grid = Grid1.of(10, 20);

    // When you try to turn on a light outside the grid
    // Then an IllegalArgumentException is thrown immediately
    assertThrows(IllegalArgumentException.class, () -> grid.turnOn(x1, y1, x2, y2));
  }

  @Test
  void turnOffAll() {
    // Given a 10*10 grid with all lights turned on
    Grid1 grid = Grid1.of(10, 10).turnOn(0, 0, 9, 9);

    // When you turn all light off and get the result
    int[][] lights = grid.turnOff(0, 0, 9, 9).get();

    // Then all light are off
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(0, i));
  }

  @Test
  void turnOffOne() {
    // Given a 10*10 grid with all lights turned on
    Grid1 grid = Grid1.of(10, 10)
                      .turnOn(0, 0, 9, 9);

    // When you turn first light off and get the result
    int[][] lights = grid.turnOff(0,0,0,0).get();

    // Then except the first one all light are on
    assertThat(lights[0][0], is(0));
    Arrays.stream(lights).flatMapToInt(Arrays::stream).skip(1).forEach(i -> assertEquals(1, i));
  }

  @Test
  void turnOffAlreadyOff() {
    // Given a 10*10 grid
    Grid1 grid = Grid1.of(10, 10);

    // When you turn off the first light already off and get the result
    int[][] lights = grid.turnOff(0, 0, 0, 0).get();

    // Then all light are off
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(0, i));
  }

  @ParameterizedTest
  @CsvSource(value = { "4,4,5,5", "5,5,4,4", "4,5,5,4", "5,4,4,5", })
  void turnOffInvarient(int x1, int y1, int x2, int y2) {
    // Given a 10*10 grid with all lights turned on
    Grid1 grid = Grid1.of(10, 10)
                      .turnOn(0, 0, 9, 9);

    // When you turn on with parameters in any order
    int[][] lights = grid.turnOff(x1, y1, x2, y2).get();

    // Then result is the same
    assertThat(countLightsOn(lights), is(96));
  }

  @ParameterizedTest
  @CsvSource(value = { "-1,0,0,0", "0,-1,0,0", "0,0,-1,0", "0,0,0,-1", "11,0,0,0", "0,21,0,0", "0,0,11,0", "0,0,0,21" })
  void turnOffOutsideGrid(int x1, int y1, int x2, int y2) {
    // Given a 10*20 grid
    Grid1 grid = Grid1.of(10, 20);

    // When you try to turn off a light outside the grid
    // Then an IllegalArgumentException is thrown immediately
    assertThrows(IllegalArgumentException.class, () -> grid.turnOff(x1, y1, x2, y2));
  }

  @Test
  void toggleAll() {
    // Given a 10*10 grid
    Grid1 grid = Grid1.of(10, 10);

    // When you toggle all light and get the result
    int[][] lights = grid.toggle(0, 0, 9, 9).get();

    // Then all light are on
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(1, i));
  }

  @Test
  void toggleOne() {
    // Given a 10*10 grid
    Grid1 grid = Grid1.of(10, 10);

    // When you toggle first light and get the result
    int[][] lights = grid.toggle(0, 0, 0, 0).get();

    // Then except the first one all light are off
    assertThat(lights[0][0], is(1));
    Arrays.stream(lights).flatMapToInt(Arrays::stream).skip(1).forEach(i -> assertEquals(0, i));
  }

  @Test
  void toggleOneTwice() {
    // Given a 10*10 grid
    Grid1 grid = Grid1.of(10, 10);

    // When you toggle first light twice and get the result
    int[][] lights = grid.toggle(0, 0, 0, 0).toggle(0, 0, 0, 0).get();

    // Then all light are off
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(0, i));
  }

  @ParameterizedTest
  @CsvSource(value = { "4,4,5,5", "5,5,4,4", "4,5,5,4", "5,4,4,5", })
  void toggleInvarient(int x1, int y1, int x2, int y2) {
    // Given a 10*10 grid
    Grid1 grid = Grid1.of(10, 10);

    // When you toggle with parameters in any order
    int[][] lights = grid.toggle(x1, y1, x2, y2).get();

    // Then result is the same
    assertThat(countLightsOn(lights), is(4));
  }

  @ParameterizedTest
  @CsvSource(value = { "-1,0,0,0", "0,-1,0,0", "0,0,-1,0", "0,0,0,-1", "11,0,0,0", "0,21,0,0", "0,0,11,0", "0,0,0,21" })
  void toggleOutsideGrid(int x1, int y1, int x2, int y2) {
    // Given a 10*20 grid
    Grid1 grid = Grid1.of(10, 20);

    // When you try to toggle a light outside the grid
    // Then an IllegalArgumentException is thrown immediately
    assertThrows(IllegalArgumentException.class, () -> grid.toggle(x1, y1, x2, y2));
  }

  private int countLightsOn(int[][] lights) {
    return Arrays.stream(lights)
                 .flatMapToInt(Arrays::stream)
                 .reduce(Math::addExact)
                 .orElse(-1);
  }

}

