package org.javakatas.gridlight;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Created by vterret on 09/08/2019.
 *
 * @author vterret
 * @version 1.0.0, 09/08/2019
 */
class Grid2Test {

  /**
   * Test creation with invalid values
   */
  @ParameterizedTest
  @CsvSource(value = { "0,10", "10,0", "-1,10", "10,-1" })
  void initialValidation(int x, int y) {
    // When a grid is created with a size less or equals to 0
    // Then an IllegalArgumentException is thrown immediately
    assertThrows(IllegalArgumentException.class, () -> Grid2.of(x, y));
  }

  /**
   * Verify correct initialization with different size
   */
  @ParameterizedTest
  @CsvSource(value = { "10,10", "10,20", "1,1", "20,10" })
  void creation(int x, int y) {
    // Given a 10*10 grid
    Grid2 grid = Grid2.of(x, y);

    // When nothing is done
    int[][] lights = grid.get();

    // Then returned grid has correct size
    assertThat(lights.length, is(x));
    assertThat(lights[0].length, is(y));

    // And all light are off
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(0, i));
  }

  @Test
  void increaseAllByTwo() {
    // Given a 10*10 grid
    Grid2 grid = Grid2.of(10, 10);

    // When you increase all light by 2 and get the result
    int[][] lights = grid.increase(0, 0, 9, 9, 2).get();

    // Then all light have brightness set to 2
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(2, i));
  }

  @Test
  void increaseOneByTwo() {
    // Given a 10*10 grid
    Grid2 grid = Grid2.of(10, 10);

    // When you increase brightness of first light by 2 and get the result
    int[][] lights = grid.increase(0, 0, 0, 0, 2).get();

    // Then except the first one all light are off
    assertThat(lights[0][0], is(2));
    Arrays.stream(lights).flatMapToInt(Arrays::stream).skip(1).forEach(i -> assertEquals(0, i));
  }

  @Test
  void increaseOverMaximum() {
    // Given a 10*10 grid with brightness set to maximum
    Grid2 grid = Grid2.of(10, 10)
                      .increase(0, 0, 9, 9, 255);

    // When you increase brightness of first light over 255 and get the result
    int[][] lights = grid.increase(0, 0, 0, 0, 1).get();

    // Then all light are still set to 255
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(255, i));
  }


  @ParameterizedTest
  @CsvSource(value = { "4,4,5,5", "5,5,4,4", "4,5,5,4", "5,4,4,5", })
  void increaseInvarient(int x1, int y1, int x2, int y2) {
    // Given a 10*10 grid
    Grid2 grid = Grid2.of(10, 10);

    // When you increase with parameters in any order
    int[][] lights = grid.increase(x1, y1, x2, y2, 2).get();

    // Then result is the same
    assertThat(countBrightness(lights), is(8));
  }

  @ParameterizedTest
  @CsvSource(value = { "-1,0,0,0", "0,-1,0,0", "0,0,-1,0", "0,0,0,-1", "11,0,0,0", "0,21,0,0", "0,0,11,0", "0,0,0,21" })
  void increaseOutsideGrid(int x1, int y1, int x2, int y2) {
    // Given a 10*20 grid
    Grid2 grid = Grid2.of(10, 20);

    // When you try to increase brightness of a light outside the grid
    // Then an IllegalArgumentException is thrown immediately
    assertThrows(IllegalArgumentException.class, () -> grid.increase(x1, y1, x2, y2, 5));
  }

  @Test
  void decreaseAllByTwo() {
    // Given a 10*10 grid with brightness set to 4
    Grid2 grid = Grid2.of(10, 10).increase(0,0,9,9,4);

    // When you decrease all light by 2 and get the result
    int[][] lights = grid.decrease(0, 0, 9, 9, 2).get();

    // Then all light have brightness set to 2
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(2, i));
  }

  @Test
  void decreaseOneByTwo() {
    // Given a 10*10 grid with brightness set to 4
    Grid2 grid = Grid2.of(10, 10).increase(0,0,9,9,4);

    // When you decrease brightness of first light by 2 and get the result
    int[][] lights = grid.decrease(0, 0, 0, 0, 2).get();

    // Then except the first one all light have brightness set to 4
    assertThat(lights[0][0], is(2));
    Arrays.stream(lights).flatMapToInt(Arrays::stream).skip(1).forEach(i -> assertEquals(4, i));
  }

  @Test
  void decreaseOverMinimum() {
    // Given a 10*10 grid
    Grid2 grid = Grid2.of(10, 10);

    // When you decrease first light on and get the result
    int[][] lights = grid.decrease(0, 0, 0, 0, 1).get();

    // Then except all light are off
    Arrays.stream(lights).flatMapToInt(Arrays::stream).forEach(i -> assertEquals(0, i));
  }

  @ParameterizedTest
  @CsvSource(value = { "4,4,5,5", "5,5,4,4", "4,5,5,4", "5,4,4,5", })
  void decreaseInvarient(int x1, int y1, int x2, int y2) {
    // Given a 10*10 grid with brightness set to 4
    Grid2 grid = Grid2.of(10, 10).increase(0,0,9,9,4);

    // When you decrease with parameters in any order
    int[][] lights = grid.decrease(x1, y1, x2, y2, 2).get();

    // Then result is the same
    assertThat(countBrightness(lights), is(392));
  }

  @ParameterizedTest
  @CsvSource(value = { "-1,0,0,0", "0,-1,0,0", "0,0,-1,0", "0,0,0,-1", "11,0,0,0", "0,21,0,0", "0,0,11,0", "0,0,0,21" })
  void decreaseOutsideGrid(int x1, int y1, int x2, int y2) {
    // Given a 10*20 grid
    Grid2 grid = Grid2.of(10, 20);

    // When you try to decrease brightness of a light outside the grid
    // Then an IllegalArgumentException is thrown immediately
    assertThrows(IllegalArgumentException.class, () -> grid.decrease(x1, y1, x2, y2, 5));
  }

  private int countBrightness(int[][] lights) {
    return Arrays.stream(lights)
                 .flatMapToInt(Arrays::stream)
                 .reduce(Math::addExact)
                 .orElse(-1);
  }

}
