# ISBN Kata

Implementation of a validator for isbn numbers

## Definition

[The International Standard Book Number (ISBN)](https://en.wikipedia.org/wiki/International_Standard_Book_Number) is a numeric commercial book identifier which is intended to be unique.
Two type of ISBN Codes exists ISBN-13 and ISBN-10 each one have a validation check described bellow.

## Features

### 1 - Check ISBN-13
 
 The rule for validating ISBN-13 code is : 
 Take the sum of each digit multiplied alternatively by 1 and by 3 (1st digit is multiplied by 1, the second by 3, the thrirs by 1, ...). 
 If the modulo of this sum by 10 is equals to 0 ths code is valid.
 
 Exemple : 9780470059029 => ((9 * 1) + (7 * 3) + (8 * 1) + (0 * 3) + (4 * 1) + (7 * 3) + (0 * 1) + (0 * 3) + (5 *1 ) + (9 * 3) + (0 * 1) + (2 * 3) + (9 * 1)) % 10 = 0
 
 Complete the [IsbnValidator](src/main/java/org/javakatas/isbncheck/IsbnValidator.java) and pass all test on [testISBN13](src/test/java/org/javakatas/isbncheck/testISBN13.java)
 
### 2 - Check ISBN-10

 The rule for validating ISBN-10 code is :
 Take the sum of each digit multiplied alternatively by 11 - position of digit (1st digit is multiplied by 10, the second by 9, the thrirs by 8, ...)
 If the modulo of this sum by 11 is equals to 0 ths code is valid.
 
 Exemple : 9780470050 => ((9 * 10) + (7 * 9) + (8 * 8) + (0 * 7) + (4 * 6) + (7 * 5) + (0 * 4) + (0 * 3) + (5 * 2) + (0 * 1)) % 11 = 0

 Complete the [IsbnValidator](src/main/java/org/javakatas/isbncheck/IsbnValidator.java) and pass all test on [testISBN10](src/test/java/org/javakatas/isbncheck/testISBN10.java)
