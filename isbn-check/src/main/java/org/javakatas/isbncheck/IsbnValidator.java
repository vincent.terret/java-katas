package org.javakatas.isbncheck;

/**
 * Created by vterret on 24/07/2019.
 *
 * @author vterret
 * @version 1.0.0, 24/07/2019
 */
@FunctionalInterface
public interface IsbnValidator {

  boolean check(String isbn);

  static IsbnValidator create() {
    return value -> false;
  }

}
