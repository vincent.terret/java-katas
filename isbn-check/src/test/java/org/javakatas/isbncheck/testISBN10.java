package org.javakatas.isbncheck;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by vterret on 24/07/2019.
 *
 * @author vterret
 * @version 1.0.0, 24/07/2019
 */
public class testISBN10 {

  @ParameterizedTest
  @NullAndEmptySource
  void testNullOrEmpty(String isbn) {
    // Given an ISBN validator
    IsbnValidator isbnValidator = IsbnValidator.create();

    // When isbn number is null or empty
    // Then validator throws an error
    Assertions.assertThrows(IllegalArgumentException.class, () -> isbnValidator.check(isbn));
  }

  @ParameterizedTest
  @CsvFileSource(resources = "isbn10-OK.csv")
  void testValidIsbn10(String isbn) {
    // Given an ISBN validator
    IsbnValidator isbnValidator = IsbnValidator.create();

    // When isbn number is valid
    boolean check = isbnValidator.check(isbn);

    // Then validator check return true
    assertTrue(check, "Valid isbn must return true");
  }

  @ParameterizedTest
  @CsvFileSource(resources = "isbn10-KO.csv")
  void testInvalidIsbn10(String isbn) {
    // Given an ISBN validator
    IsbnValidator isbnValidator = IsbnValidator.create();

    // When isbn number is valid
    boolean check = isbnValidator.check(isbn);

    // Then validator check return true
    assertFalse(check, "Invalid isbn must return false");
  }

}
